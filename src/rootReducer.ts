import { combineReducers } from '@reduxjs/toolkit';
import skillsReducer from './redux/skillSlice/redux';
import sidebarReducer from './redux/sidebarSlice/redux';
import educationReducer from './redux/educationSlice/redux';

const rootReducer = combineReducers({
	sidebar: sidebarReducer,
	education: educationReducer,
	skills: skillsReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
