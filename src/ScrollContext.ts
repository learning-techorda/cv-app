import React from 'react';

interface ScrollContextType {
	scrollToSection: (id: string) => void;
}

export const ScrollContext = React.createContext<ScrollContextType>({
	scrollToSection: () => {},
});
