export interface ReviewProps {
	review: string;
	pathToImg: string;
	reviewerName: string;
	reviewerEmail: string;
	dataTestId: string;
}
