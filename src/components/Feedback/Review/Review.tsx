import React, { FC } from 'react';
import style from './Review.module.scss';
import { ReviewProps } from './types';

const Review: FC<ReviewProps> = ({
	review,
	pathToImg,
	reviewerName,
	reviewerEmail,
	dataTestId,
}) => {
	return (
		<ul className={style.reviewCard} data-testid={dataTestId}>
			<div className={style.box}>
				<li className={style.review}>{review}</li>
				<div className={style.container}>
					<img className={style.img} src={pathToImg} alt='reviewer image' />
					<p className={style.reviewerName}>
						{reviewerName}
						{', '}
						<a
							className={style.reviewerEmail}
							href={reviewerEmail}
							target='_blank'
						>
							{reviewerEmail}
						</a>
					</p>
				</div>
			</div>
		</ul>
	);
};

export default Review;
