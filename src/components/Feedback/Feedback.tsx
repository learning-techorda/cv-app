import React from 'react';
import reviewerImage from 'src/assets/images/feedback/reviewerImage.png';
import Review from './Review/Review';
import style from './Feedback.module.scss';

export const feedbackList = [
	{
		review:
			'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ',
		pathToImg: reviewerImage,
		reviewerName: 'Martin Friman Programmer',
		reviewerEmail: 'somesite.com',
	},
	{
		review:
			'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ',
		pathToImg: reviewerImage,
		reviewerName: 'Martin Friman Programmer',
		reviewerEmail: 'somesite.com',
	},
];

const Feedback = () => {
	return (
		<div className={style.reviewContainer}>
			<h1 className={style.header}>Feedback</h1>
			{feedbackList.map((list, index) => (
				<Review
					key={index}
					review={list.review}
					pathToImg={list.pathToImg}
					reviewerName={list.reviewerName}
					reviewerEmail={list.reviewerEmail}
					dataTestId={`reviewComponent-${index}`}
				/>
			))}
		</div>
	);
};

export default Feedback;
