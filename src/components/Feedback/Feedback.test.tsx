import React from 'react';
import { render, screen, within } from '@testing-library/react';
import Feedback, { feedbackList } from './Feedback';
import '@testing-library/jest-dom';

describe('Feedback Component', () => {
	it('renders the review components for each entry', () => {
		render(<Feedback />);
		feedbackList.forEach((item, index) => {
			const reviewComponent = screen.getByTestId(`reviewComponent-${index}`);

			expect(reviewComponent).toBeInTheDocument();

			const image = within(reviewComponent).getByRole('img');
			expect(image).toHaveAttribute('src', item.pathToImg);
		});
	});
});
