import React, { FC, CSSProperties } from 'react';

interface ButtonProps {
	className: string;
	onClick?: () => void;
	children?: React.ReactNode;
	style?: CSSProperties;
	ref?: string;
	type?: 'button' | 'submit' | 'reset';
	disabled?: boolean;
	id?: string;
}

const Button: FC<ButtonProps> = ({
	className,
	onClick,
	children,
	style,
	type = 'button',
	disabled,
	id,
}) => {
	return (
		<button
			className={className}
			type={type}
			onClick={onClick}
			style={style}
			disabled={disabled}
			id={id}
		>
			{children}
		</button>
	);
};
export default Button;
