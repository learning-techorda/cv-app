import React, { FC, useEffect, useState, useContext } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from 'src/store';
import { ScrollContext } from 'src/ScrollContext';
import { NavigationElementProps } from './types';

const NavigationElement: FC<NavigationElementProps> = ({
	styleContainer,
	styleImg,
	imgPath,
	alt,
	linkValue,
	linkStyle,
	navigationElement,
	linkId,
}) => {
	const isSidebarExpanded = useSelector(
		(state: RootState) => state.sidebar.isSidebarExpanded
	);

	const [showLink, setShowLink] = useState(isSidebarExpanded);

	useEffect(() => {
		setShowLink(isSidebarExpanded);
	}, [isSidebarExpanded]);

	const { scrollToSection } = useContext(ScrollContext);

	return (
		<ul className={navigationElement}>
			<li className={styleContainer} onClick={() => scrollToSection(linkId)}>
				{showLink && (
					<>
						<img className={styleImg} src={imgPath} alt={alt} />
						<span className={linkStyle}>{linkValue} </span>
					</>
				)}
			</li>
		</ul>
	);
};

export default NavigationElement;
