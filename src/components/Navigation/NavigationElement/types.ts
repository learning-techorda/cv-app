export interface NavigationElementProps {
	styleContainer: string;
	styleImg: string;
	imgPath: string;
	alt: string;
	linkPath?: string;
	linkValue?: string;
	linkStyle?: string;
	navigationElement: string;
	linkId: string;
}
