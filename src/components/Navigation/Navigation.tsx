import React from 'react';
import style from './Navigation.module.scss';
import NavigationElement from './NavigationElement/NavigationElement';
import aboutImg from 'src/assets/images/navigation/About.png';
import educationImg from 'src/assets/images/navigation/Education.png';
import experienceImg from 'src/assets/images/navigation/Experience.png';
import skillsImg from 'src/assets/images/navigation/Skills.png';
import portfolioImg from 'src/assets/images/navigation/Portfolio.png';
import contactsImg from 'src/assets/images/navigation/Vector.png';
import feedbackImg from 'src/assets/images/navigation/Feedbacks.png';

const Navigation = () => {
	return (
		<div className={style.navigationContainer}>
			<div className={style.sidebar}>
				<NavigationElement
					styleContainer={style.element}
					styleImg={style.img}
					imgPath={aboutImg}
					linkId='aboutMeSection'
					alt='about'
					linkValue='About me'
					linkStyle={style.linkStyle}
					navigationElement={style.navigationElement}
				/>
				<NavigationElement
					styleContainer={style.element}
					styleImg={style.img}
					imgPath={educationImg}
					linkId='educationSection'
					alt='education'
					linkValue='Education'
					linkStyle={style.linkStyle}
					navigationElement={style.navigationElement}
				/>
				<NavigationElement
					styleContainer={style.element}
					styleImg={style.img}
					imgPath={experienceImg}
					alt='experience'
					linkId='experienceSection'
					linkValue='Experience'
					linkStyle={style.linkStyle}
					navigationElement={style.navigationElement}
				/>
				<NavigationElement
					styleContainer={style.element}
					styleImg={style.img}
					imgPath={skillsImg}
					alt='skills'
					linkId='skillsSection'
					linkValue='Skills'
					linkStyle={style.linkStyle}
					navigationElement={style.navigationElement}
				/>
				<NavigationElement
					styleContainer={style.element}
					styleImg={style.img}
					imgPath={portfolioImg}
					alt='portfolio'
					linkId='portfolioSection'
					linkValue='Portfolio'
					linkStyle={style.linkStyle}
					navigationElement={style.navigationElement}
				/>
				<NavigationElement
					styleContainer={style.element}
					styleImg={style.img}
					imgPath={contactsImg}
					alt='contacts'
					linkId='contactsSection'
					linkValue='Contacts'
					linkStyle={style.linkStyle}
					navigationElement={style.navigationElement}
				/>
				<NavigationElement
					styleContainer={style.element}
					styleImg={style.img}
					imgPath={feedbackImg}
					linkId='feedbackSection'
					alt='feedback'
					linkValue='Feedback'
					linkStyle={style.linkStyle}
					navigationElement={style.navigationElement}
				/>
			</div>
		</div>
	);
};

export default Navigation;
