import React from 'react';
import { render } from '@testing-library/react';
import Navigation from './Navigation';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import rootReducer from '../../rootReducer';

const store = configureStore({
	reducer: rootReducer,
	preloadedState: {
		sidebar: { isSidebarExpanded: true },
	},
});

describe('Navigation', () => {
	it('renders all navigation elements correctly', () => {
		const { getByText, getAllByRole } = render(
			<Provider store={store}>
				<Navigation />
			</Provider>
		);

		expect(getByText('About me')).toBeInTheDocument();
		expect(getByText('Education')).toBeInTheDocument();
		expect(getByText('Experience')).toBeInTheDocument();
		expect(getByText('Skills')).toBeInTheDocument();
		expect(getByText('Portfolio')).toBeInTheDocument();
		expect(getByText('Contacts')).toBeInTheDocument();
		expect(getByText('Feedback')).toBeInTheDocument();

		const images = getAllByRole('img');
		expect(images.length).toBe(7);

		expect(images[0]).toHaveAttribute('alt', 'about');
		expect(images[1]).toHaveAttribute('alt', 'education');
		expect(images[2]).toHaveAttribute('alt', 'experience');
		expect(images[3]).toHaveAttribute('alt', 'skills');
		expect(images[4]).toHaveAttribute('alt', 'portfolio');
		expect(images[5]).toHaveAttribute('alt', 'contacts');
		expect(images[6]).toHaveAttribute('alt', 'feedback');
	});
});
