export interface EducationEntryProps {
	date: string;
	title: string;
	description: string;
}
