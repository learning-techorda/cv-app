import React from 'react';
import style from './LoaderSpinner.module.scss';
import spinner from 'src/assets/images/loading/loader-spinner.svg';

const LoaderSpinner = () => {
	return (
		<div className={style.loader}>
			<img className={style.spinner} src={spinner} alt='Loading' />
		</div>
	);
};

export default LoaderSpinner;
