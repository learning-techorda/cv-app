import React, { FC } from 'react';
import style from './ErrorMessage.module.scss';
import { errorMessageProps } from './types';
const errorMessage: FC<errorMessageProps> = ({ message }) => {
	return (
		<div className={style.container}>
			<p className={style.error}>{message}</p>
		</div>
	);
};

export default errorMessage;
