import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import EducationList from './EducationList';
import { thunk } from 'redux-thunk';
import educationsSlice from '../../../redux/educationSlice/redux';

const mockStore = configureStore({
	reducer: {
		education: educationsSlice,
	},
	middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(thunk),
});

describe('EducationList Component', () => {
	it('renders without crashing', () => {
		const { getByText } = render(
			<Provider store={mockStore}>
				<EducationList />
			</Provider>
		);

		expect(getByText('Education')).toBeInTheDocument();
	});
});
