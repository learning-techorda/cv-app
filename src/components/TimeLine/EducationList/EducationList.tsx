import React, { FC, useEffect } from 'react';
import TimeLine from '../TimeLine';
import ErrorMessage from '../ErrorMessage/ErrorMessage';
import LoaderSpinner from '../LoaderSpinner/LoaderSpinner';
import style from './EducationList.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../store';
import { fetchEducations } from '../../../redux/educationSlice/thunk';

const EducationList: FC = () => {
	const dispatch = useDispatch<AppDispatch>();
	const educationData = useSelector(
		(state: RootState) => state.education.educations
	);
	const isLoading = useSelector((state: RootState) => state.education.loading);
	const error = useSelector((state: RootState) => state.education.error);

	useEffect(() => {
		dispatch(fetchEducations());
	}, [dispatch]);

	return (
		<div className={style.educationBox}>
			<h2 className={style.header}>Education</h2>
			<div className={style.scrollBox}>
				{isLoading ? (
					<div className={style.loaderBox}>
						<LoaderSpinner />
					</div>
				) : error ? (
					<ErrorMessage message={error} />
				) : (
					educationData.map((list, index) => (
						<div className={style.timelineContainer} key={index}>
							<TimeLine
								date={list.date}
								title={list.title}
								description={list.description}
							/>
						</div>
					))
				)}
			</div>
		</div>
	);
};

export default EducationList;
