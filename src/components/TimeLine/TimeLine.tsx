import React, { FC } from 'react';
import { EducationEntryProps } from './types';
import style from './TimeLine.module.scss';
const TimeLine: FC<EducationEntryProps> = ({ date, title, description }) => {
	return (
		<div className={style.timeLineBox}>
			<div className={style.container}>
				<div className={style.yearContainer}>
					<div className={style.year}>{date}</div>
					<div className={style.line}></div>
				</div>
				<div className={style.details}>
					<div className={style.triangle}></div>
					<div className={style.infoContainer}>
						<h3 className={style.title}>{title}</h3>
						<p className={style.description}>{description}</p>
					</div>
				</div>
			</div>
		</div>
	);
};

export default TimeLine;
