export interface PhotoBoxProps {
	name?: string;
	title?: string;
	description?: string;
	avatar?: string;
	avatarClass?: string;
	nameClass?: string;
	titleClass?: string;
	descriptionClass?: string;
}
