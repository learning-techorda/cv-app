import React from 'react';
import { PhotoBoxProps } from './types';

const PhotoBox: React.FC<PhotoBoxProps> = ({
	name,
	title,
	description,
	avatar,
	avatarClass,
	nameClass,
	titleClass,
	descriptionClass,
}) => {
	return (
		<>
			{avatar && <img className={avatarClass} src={avatar} alt='Avatar' />}
			{name && <h1 className={nameClass}>{name}</h1>}
			{title && <h2 className={titleClass}>{title}</h2>}
			{description && <p className={descriptionClass}>{description}</p>}
		</>
	);
};

export default PhotoBox;
