import React from 'react';
import { render, screen } from '@testing-library/react';
import PhotoBox from './PhotoBox';

describe('PhotoBox Component', () => {
	test('renders correctly with all props provided', () => {
		const props = {
			name: 'John Doe',
			title: 'Developer',
			description: 'Writes code.',
			avatar: 'path/to/avatar.jpg',
			avatarClass: 'avatar-style',
			nameClass: 'name-style',
			titleClass: 'title-style',
			descriptionClass: 'description-style',
		};

		render(<PhotoBox {...props} />);

		expect(screen.getByText('John Doe')).toHaveClass('name-style');
		expect(screen.getByText('Developer')).toHaveClass('title-style');
		expect(screen.getByText('Writes code.')).toHaveClass('description-style');

		const avatar = screen.getByAltText('Avatar') as HTMLImageElement;
		expect(avatar).toBeInTheDocument();
		expect(avatar).toHaveClass('avatar-style');
		expect(avatar.src).toContain('path/to/avatar.jpg');
	});
});
