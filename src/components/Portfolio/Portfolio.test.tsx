import React from 'react';
import { render, screen } from '@testing-library/react';
import Portfolio from './Portfolio';
import '@testing-library/jest-dom';

describe('Portfolio Component', () => {
	it('renders and displays the main components', () => {
		render(<Portfolio />);

		expect(screen.getByText('Portfolio')).toBeInTheDocument();

		// Проверяем, что кнопки для фильтрации отображаются
		expect(screen.getByText('All')).toBeInTheDocument();
		expect(screen.getByText('UI')).toBeInTheDocument();
		expect(screen.getByText('Code')).toBeInTheDocument();

		const cards = screen.getAllByText(/Some text/i);
		expect(cards.length).toBeGreaterThanOrEqual(4);
	});
});
