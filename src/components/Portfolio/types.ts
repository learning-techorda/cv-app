export type CardProps = {
	image: string;
	text: string;
	sourceUrl: string;
	category: string;
	title: string;
};

export type ItemListProps = {
	id: string;
	category: string;
	image: string;
	text: string;
	sourceUrl: string;
	title: string;
};
