import React, { FC } from 'react';
import style from './Card.module.scss';
import { CardProps } from './types';

const Card: FC<CardProps> = ({ image, text, sourceUrl, title, category }) => {
	return (
		<div className={`isotope-card ${category}`}>
			<div className={`${style.cardContainer}`}>
				<div className={style.imageBlock}>
					<img className={style.img} src={image} alt='Portfolio example' />
				</div>
				<div className={style.overlay}>
					<div className={style.overlayBox}>
						<p className={style.title}>{title}</p>
						<p className={style.text}>{text}</p>
						<a className={style.url} href={sourceUrl}>
							View source
						</a>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Card;
