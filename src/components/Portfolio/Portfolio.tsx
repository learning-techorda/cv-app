import React, { useEffect, useRef, useState } from 'react';
import style from './Portfolio.module.scss';
import Card from './Card';
import Button from '../Button/Button';
import Isotope from 'isotope-layout';
import { ItemListProps } from './types';
import card1 from '../../assets/images/portfolio/Portfolio cards.png';
import card2 from '../../assets/images/portfolio/Portfolio cards (1).png';
import cardStyle from './Card.module.scss';
import { debounce } from 'lodash';
import imagesLoaded from 'imagesloaded';

const itemList: ItemListProps[] = [
	{
		id: '1',
		category: 'ui',
		image: card1,
		title: 'Some text',
		text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis',
		sourceUrl: '#',
	},
	{
		id: '2',
		category: 'code',
		image: card2,
		title: 'Some text',
		text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis',
		sourceUrl: '#',
	},
	{
		id: '3',
		category: 'ui',
		image: card1,
		title: 'Some text',
		text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis',
		sourceUrl: '#',
	},
	{
		id: '4',
		category: 'code',
		image: card2,
		title: 'Some text',
		text: 'Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis',
		sourceUrl: '#',
	},
];

const Portfolio = () => {
	const isotopeRef = useRef(null);
	const gridRef = useRef(null);
	const [filterKey, setFilterKey] = useState('*');

	useEffect(() => {
		if (!isotopeRef.current && gridRef.current) {
			isotopeRef.current = new Isotope(gridRef.current, {
				itemSelector: `.isotope-card`,
				layoutMode: 'fitRows',
				initLayout: false,
			});
		}
		return () => {
			isotopeRef.current && isotopeRef.current.destroy();
		};
	}, [gridRef, cardStyle.card]);

	useEffect(() => {
		if (isotopeRef.current) {
			isotopeRef.current.arrange({
				filter: filterKey === '*' ? '*' : `.${filterKey}`,
			});
		}
	}, [filterKey]);

	useEffect(() => {
		const handleResize = debounce(() => {
			if (gridRef.current) {
				if (isotopeRef.current) {
					isotopeRef.current.destroy();
				}
				imagesLoaded(gridRef.current, () => {
					isotopeRef.current = new Isotope(gridRef.current, {
						itemSelector: '.isotope-card',
						layoutMode: 'fitRows',
					});
					console.log('Isotope reinitialized');
				});
			}
		}, 100);

		window.addEventListener('resize', handleResize);
		return () => {
			window.removeEventListener('resize', handleResize);
		};
	}, []);

	return (
		<div className={style.portfolioContainer}>
			<h1 className={style.header}>Portfolio</h1>
			<div className={style.buttonsBox}>
				<Button className={style.btn} onClick={() => setFilterKey('*')}>
					All
				</Button>
				{'/ '}
				<Button className={style.btn} onClick={() => setFilterKey('ui')}>
					UI
				</Button>
				{'/ '}
				<Button className={style.btn} onClick={() => setFilterKey('code')}>
					Code
				</Button>
			</div>
			<div ref={gridRef} className={style.grid}>
				{itemList.map((item) => (
					<Card
						key={item.id}
						image={item.image}
						title={item.title}
						text={item.text}
						sourceUrl={item.sourceUrl}
						category={item.category}
					/>
				))}
			</div>
		</div>
	);
};

export default Portfolio;
