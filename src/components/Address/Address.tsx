import React from 'react';
import style from './Address.module.scss';
import ContactsCard from './ContactsCard/ContactsCard';

const Address = () => {
	return (
		<div className={style.addressContainer}>
			<h1 className={style.header}>Contacts</h1>
			<ContactsCard
				phone='500 342 242'
				email='office@kamsolutions.pl'
				twitter='https://twitter.com/wordpress'
				facebook='https://www.facebook.com/facebook'
				skype='kamsolutions.pl'
			/>
		</div>
	);
};

export default Address;
