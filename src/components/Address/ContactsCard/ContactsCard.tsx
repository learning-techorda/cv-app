import React, { FC } from 'react';
import emailIcon from 'src/assets/images/address/envelope-solid.svg';
import phoneIcon from 'src/assets/images/address/phone-solid.svg';
import twitterIcon from 'src/assets/images/address/twitter.svg';
import facebookIcon from 'src/assets/images/address/facebook-f.svg';
import skypeIcon from 'src/assets/images/address/skype.svg';
import { ContactsCardTypes } from './ContactsCard.types';
import style from './ContactsCard.module.scss';

const ContactsCard: FC<ContactsCardTypes> = ({
	phone,
	email,
	twitter,
	facebook,
	skype,
}) => {
	return (
		<div className={style.contactsCard}>
			<div className={style.box}>
				<img className={style.icon} src={phoneIcon} alt='phone icon' />
				<a className={style.link} href={`tel:${phone}`}>
					{phone}
				</a>
			</div>
			<div className={style.box}>
				<img className={style.icon} src={emailIcon} alt='email icon' />
				<a className={style.link} href={`mailto:${email}`}>
					{email}
				</a>
			</div>
			<div className={style.box}>
				<img className={style.icon} src={twitterIcon} alt='twitter icon' />
				<div className={style.subBlock}>
					<h2>Twitter</h2>
					<a
						className={`${style.link} ${style.addition}`}
						href={twitter}
						target='_blank'
					>
						{twitter}
					</a>
				</div>
			</div>
			<div className={style.box}>
				<img className={style.icon} src={facebookIcon} alt='facebook icon' />
				<div className={style.subBlock}>
					<h2>Facebook</h2>
					<a
						className={`${style.link} ${style.addition} ${style.facebook}`}
						href={facebook}
						target='_blank'
					>
						{facebook}
					</a>
				</div>
			</div>
			<div className={style.box}>
				<img className={style.icon} src={skypeIcon} alt='skype icon' />
				<div className={style.subBlock}>
					<h2>Facebook</h2>
					<a
						className={`${style.link} ${style.addition}`}
						href={`skype:${skype}?call`}
					>
						{skype}
					</a>
				</div>
			</div>
		</div>
	);
};

export default ContactsCard;
