export interface ContactsCardTypes {
	phone: string;
	email: string;
	twitter: string;
	facebook: string;
	skype: string;
}
