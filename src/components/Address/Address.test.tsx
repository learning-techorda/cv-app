import React from 'react';
import { render } from '@testing-library/react';
import Address from './Address';

describe('Address Component', () => {
	it('renders the contacts header and details', () => {
		const { getByText } = render(<Address />);

		expect(getByText('Contacts')).toBeInTheDocument();

		expect(getByText('500 342 242')).toBeInTheDocument();
		expect(getByText('office@kamsolutions.pl')).toBeInTheDocument();
		expect(getByText('https://twitter.com/wordpress')).toBeInTheDocument();
		expect(getByText('https://www.facebook.com/facebook')).toBeInTheDocument();
		expect(getByText('kamsolutions.pl')).toBeInTheDocument();
	});
});
