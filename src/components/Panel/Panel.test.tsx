import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Panel from './Panel';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';

const rootReducer = (state = { sidebar: { isSidebarExpanded: false } }) =>
	state;

const store = configureStore({
	reducer: rootReducer,
});

describe('Panel Component', () => {
	it('renders and shows basic components', () => {
		render(
			<Provider store={store}>
				<MemoryRouter>
					<Panel />
				</MemoryRouter>
			</Provider>
		);

		expect(screen.getByAltText('burgerButton')).toBeInTheDocument();

		const avatar = screen.queryByAltText('User avatar');
		if (avatar) {
			expect(avatar).toBeVisible();
		}

		const navigation = screen.queryByText('Navigation placeholder');
		if (navigation) {
			expect(navigation).toBeVisible();
		}

		const goBackButton = screen.queryByText('Go back');
		if (goBackButton) {
			expect(goBackButton).toBeVisible();
		}
	});
});
