import React, { useEffect, useState } from 'react';
import style from './Panel.module.scss';
import Navigation from '../Navigation/Navigation';
import PhotoBox from '../PhotoBox/PhotoBox';
import Button from '../Button/Button';
import burgerButton from 'src/assets/images/panel/bars-solid.svg';
import user from 'src/assets/images/photoBox/User avatar.png';
import { useNavigate } from 'react-router';
import { changeSidebar } from 'src/redux/sidebarSlice/redux';
import { RootState } from 'src/store';
import { useDispatch, useSelector } from 'react-redux';
import goBackImageButton from 'src/assets/images/navigation/angle-left-solid.svg';

const Panel = () => {
	const dispatch = useDispatch();
	const isChangedSize = useSelector(
		(state: RootState) => state.sidebar.isSidebarExpanded
	);
	const [showBox, setShowBox] = useState(false);
	const [opacity, setOpacity] = useState(0);
	const [elementsVisible, setElementsVisible] = useState(false);

	const navigate = useNavigate();
	const name = 'John Doe';

	useEffect(() => {
		const timer = setTimeout(() => {
			setShowBox(isChangedSize);
			setOpacity(isChangedSize ? 1 : 0);
		}, 100);
		return () => clearTimeout(timer);
	}, [isChangedSize]);

	useEffect(() => {
		if (isChangedSize) {
			const timer = setTimeout(() => {
				setElementsVisible(true);
			}, 200);
			return () => clearTimeout(timer);
		} else {
			const timer = setTimeout(() => {
				setElementsVisible(false);
			}, 250);
			return () => clearTimeout(timer);
		}
	}, [isChangedSize]);

	const goBackButton = () => {
		navigate('/');
	};
	const toggleSidebar = () => {
		dispatch(changeSidebar());
	};

	const PhotoBoxAvatarSize = () => {
		return isChangedSize ? style.avatarLarge : style.avatarSmall;
	};

	return (
		<div className={isChangedSize ? style.panelWide : style.panelThin}>
			<div className={style.container}>
				<Button
					onClick={toggleSidebar}
					className={
						isChangedSize ? style.burgerBtnChanged : style.burgerBtnNormal
					}
				>
					<img
						className={style.burgerButtonIcon}
						src={burgerButton}
						alt='burgerButton'
					/>
				</Button>
				{elementsVisible && (
					<div className={style.photoBoxContainer}>
						<PhotoBox avatar={user} avatarClass={PhotoBoxAvatarSize()} />
						{showBox && (
							<div
								style={{
									opacity: opacity,
									transition: 'opacity 0.8s ease-in-out',
								}}
							>
								<PhotoBox name={name} nameClass={style.name} />
							</div>
						)}
					</div>
				)}
				{elementsVisible && (
					<div className={style.navigation}>
						<Navigation />
					</div>
				)}
				{elementsVisible && (
					<div className={style.goBackBlock}>
						<Button className={style.goBackBtn} onClick={goBackButton}>
							<div className={style.goBackContainer}>
								<img
									className={style.goBackIcon}
									src={goBackImageButton}
									alt='Go back icon'
								/>
								<p className={style.goBackIconTitle}>Go back</p>
							</div>
						</Button>
					</div>
				)}
			</div>
		</div>
	);
};

export default Panel;
