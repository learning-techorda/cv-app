import React, { ChangeEvent, FC } from 'react';

interface InputProps {
	htmlFor: string;
	label: string;
	value: string;
	handleChange: (event: ChangeEvent<HTMLInputElement>) => void;
	placeholder: string;
	type: string;
	error: string;
	styleInput: string;
	styleLabel: string;
	style?: React.CSSProperties;
}
const Input: FC<InputProps> = ({
	htmlFor,
	label,
	handleChange,
	placeholder,
	type,
	styleInput,
	styleLabel,
	style,
	value,
}) => {
	return (
		<label className={styleLabel} htmlFor={htmlFor}>
			{label}
			<input
				className={styleInput}
				style={style}
				id={htmlFor}
				type={type}
				onChange={handleChange}
				value={value}
				placeholder={`Enter skill ${placeholder}`}
			/>
		</label>
	);
};

export default Input;
