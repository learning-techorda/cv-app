import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Input from './Input';

describe('Input Component', () => {
	const mockHandleChange = jest.fn();

	beforeEach(() => {
		render(
			<Input
				htmlFor='test-input'
				label='Test Label'
				value='Test Value'
				handleChange={mockHandleChange}
				placeholder='Skill'
				type='text'
				error=''
				styleInput='input-class'
				styleLabel='label-class'
			/>
		);
	});

	it('renders with correct label and input', () => {
		expect(screen.getByLabelText('Test Label')).toBeInTheDocument();
		expect(screen.getByLabelText('Test Label')).toHaveAttribute(
			'value',
			'Test Value'
		);
	});

	it('displays the correct placeholder', () => {
		expect(
			screen.getByPlaceholderText('Enter skill Skill')
		).toBeInTheDocument();
	});

	it('applies the correct classes', () => {
		const label = screen.getByText('Test Label');
		expect(label).toHaveClass('label-class');
		const input = screen.getByLabelText('Test Label');
		expect(input).toHaveClass('input-class');
	});
});
