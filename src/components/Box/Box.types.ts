export interface BoxProps {
	header: string;
	paragraph: string;
}
