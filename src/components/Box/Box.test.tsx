import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Box from './Box';

describe('Box Component', () => {
	it('renders the header and paragraph passed as props', () => {
		const testHeader = 'Test Header';
		const testParagraph = 'Test paragraph content';

		render(<Box header={testHeader} paragraph={testParagraph} />);

		expect(screen.getByText(testHeader)).toBeInTheDocument();
		expect(
			screen.getByRole('heading', { name: testHeader })
		).toBeInTheDocument();

		expect(screen.getByText(testParagraph)).toBeInTheDocument();
		expect(screen.getByText(testParagraph).tagName).toBe('P');
	});
});
