import React, { FC } from 'react';
import { BoxProps } from './Box.types';
import style from './Box.module.scss';

const Box: FC<BoxProps> = ({ header, paragraph }) => {
	return (
		<div className={style.container}>
			<h1 className={style.header}>{header}</h1>
			<p className={style.paragraph}>{paragraph}</p>
		</div>
	);
};

export default Box;
