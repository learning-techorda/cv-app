import React from 'react';
import ExperienceCard from './ExperienceCard/ExperienceCard';
import style from './Expertise.module.scss';

export const expertiseData = [
	{
		date: '2013-2014',
		info: {
			company: 'Google',
			job: 'Front-end developer / php programmer',
			description:
				'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringil',
		},
	},
	{
		date: '2012',
		info: {
			company: 'Twitter',
			job: 'Web developer',
			description:
				'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringil',
		},
	},
];

const Expertise = () => {
	return (
		<div className={style.expertiseBox}>
			<h1 className={style.header}>Experience</h1>
			{expertiseData.map((list, index) => (
				<ExperienceCard
					key={index}
					company={list.info.company}
					job={list.info.job}
					description={list.info.description}
					date={list.date}
					testid='experienceCard'
				/>
			))}
		</div>
	);
};

export default Expertise;
