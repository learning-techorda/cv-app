import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Expertise, { expertiseData } from './Expertise';

describe('Expertise Component', () => {
	it('renders the ExperienceCard components for each data entry', () => {
		render(<Expertise />);
		const cards = screen.getAllByTestId('experienceCard');
		expect(cards).toHaveLength(2);

		cards.forEach((card, index) => {
			expect(
				screen.getByText(expertiseData[index].info.company)
			).toBeInTheDocument();
			expect(
				screen.getByText(expertiseData[index].info.job)
			).toBeInTheDocument();

			const descriptions = screen.getAllByText(
				new RegExp(expertiseData[index].info.description.slice(0, 30), 'i')
			);
			descriptions.forEach((description) => {
				expect(description).toBeInTheDocument();
			});

			expect(screen.getByText(expertiseData[index].date)).toBeInTheDocument();
		});
	});
});
