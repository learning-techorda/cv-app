import React, { FC } from 'react';
import style from './ExperienceCard.module.scss';
import { ExperienceCardProps } from './types';

const ExperienceCard: FC<ExperienceCardProps> = ({
	company,
	job,
	description,
	date,
	testid,
}) => {
	return (
		<div className={style.experienceCard} data-testid={testid}>
			<div className={style.workPlace}>
				<div className={style.company}>{company}</div>
				<div className={style.date}>{date}</div>
			</div>
			<div className={style.position}>
				<div className={style.job}>{job}</div>
				<div className={style.description}>{description}</div>
			</div>
		</div>
	);
};

export default ExperienceCard;
