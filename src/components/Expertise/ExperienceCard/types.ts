export interface ExperienceCardProps {
	company: string;
	job: string;
	description: string;
	date: string;
	testid: string;
}
