import React, { useEffect, useState } from 'react';
import style from './SkillBlock.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { deleteSkill, fetchSkills } from 'src/redux/skillSlice/thunk';
import { AppDispatch, RootState } from 'src/store';
import Button from '../../Button/Button';
import deleteSkillButton from '../../../assets/images/skills/Red-X-PNG-Cutout.png';

const SkillBlock = () => {
	const dispatch = useDispatch<AppDispatch>();
	const skills = useSelector((state: RootState) => state.skills);
	const [activeSkill, setActiveSkill] = useState<number | null>(null);
	const [deletingIds, setDeletingIds] = useState(new Set());

	useEffect(() => {
		dispatch(fetchSkills());
	}, [dispatch]);

	const handleClick = (skillId: number) => {
		setActiveSkill(skillId === activeSkill ? null : skillId);
	};

	const handleDelete = (skillId: number) => {
		setDeletingIds((prev) => new Set(prev).add(skillId));
		dispatch(deleteSkill(skillId));
		setActiveSkill(null);
	};

	return (
		<div className={style.container}>
			{skills && skills.length > 0 ? (
				skills.map((skill) => (
					<div
						key={skill.id}
						className={`${style.block} ${skill.id === activeSkill ? style.active : ''} ${deletingIds.has(skill.id) ? style.deleting : ''}`}
						style={{ width: `${skill.range}%` }}
						onClick={() => handleClick(skill.id)}
					>
						<span className={style.nameSkill}>{skill.name}</span>
						{activeSkill === skill.id && (
							<Button
								onClick={() => handleDelete(skill.id)}
								className={style.deleteButton}
							>
								<img
									className={style.deleteSkillButton}
									src={deleteSkillButton}
									alt='deleteSkillButton'
								/>
							</Button>
						)}
					</div>
				))
			) : (
				<p>No skills. Please open the editor and add some.</p>
			)}
		</div>
	);
};

export default SkillBlock;
