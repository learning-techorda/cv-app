import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { deleteSkill, fetchSkills } from 'src/redux/skillSlice/thunk';
import SkillBlock from './SkillBlock';

const mockStore = configureStore([]);
const initialState = {
	skills: [
		{ id: 1, name: 'JavaScript', range: 80 },
		{ id: 2, name: 'React', range: 70 },
	],
};

jest.mock('src/redux/skillSlice/thunk', () => ({
	fetchSkills: jest.fn(),
	deleteSkill: jest.fn(() => (dispatch) => dispatch),
}));

describe('SkillBlock Component', () => {
	let store;

	beforeEach(() => {
		store = mockStore(initialState);
		store.dispatch = jest.fn();
	});

	test('renders skills and handles click events', () => {
		render(
			<Provider store={store}>
				<SkillBlock />
			</Provider>
		);

		expect(screen.getByText('JavaScript')).toBeInTheDocument();
		expect(screen.getByText('React')).toBeInTheDocument();

		fireEvent.click(screen.getByText('JavaScript'));
		expect(screen.getByText('JavaScript').parentElement).toHaveClass('active');

		expect(screen.getByAltText('deleteSkillButton')).toBeInTheDocument();

		fireEvent.click(screen.getByAltText('deleteSkillButton'));
		expect(deleteSkill).toHaveBeenCalledWith(1);
		expect(store.dispatch).toHaveBeenCalled();
	});

	test('dispatches fetchSkills on mount', () => {
		render(
			<Provider store={store}>
				<SkillBlock />
			</Provider>
		);

		expect(store.dispatch).toHaveBeenCalledWith(fetchSkills());
	});
});
