import React from 'react';
import style from './SkillLevel.module.scss';
const SkillLevel = () => {
	return (
		<div className={style.skillScale}>
			<div
				className={style.marker}
				style={{ left: '0%', transform: `translateX(0%)` }}
			>
				<span className={style.beginerSpan}>Beginner</span>
			</div>
			<div
				className={style.marker}
				style={{ left: '33%', transform: `translateX(-50%)` }}
			>
				<span>Proficient</span>
			</div>
			<div
				className={style.marker}
				style={{ left: '66%', transform: `translateX(-50%)` }}
			>
				<span>Expert</span>
			</div>
			<div
				className={style.marker}
				style={{ left: '100%', transform: 'translateX(-100%)' }}
			>
				<span className={style.masterSpan}>Master</span>
			</div>
		</div>
	);
};

export default SkillLevel;
