import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Skills from './Skills';

jest.mock('./SkillEdit/SkillEdit', () => () => <div>SkillEdit component</div>);
jest.mock('./SkillLevel/SkillLevel', () => () => (
	<div>SkillLevel component</div>
));
jest.mock('./SkillBlock/SkillBlock', () => () => (
	<div>SkillBlock component</div>
));

describe('Skills Component', () => {
	it('renders correctly and toggles edit mode', () => {
		render(<Skills />);

		expect(screen.getByText('Skills')).toBeInTheDocument();
		expect(screen.getByText('Open edit')).toBeInTheDocument();

		fireEvent.click(screen.getByText('Open edit'));
		expect(screen.getByText('Close edit')).toBeInTheDocument();
		expect(screen.getByText('SkillEdit component')).toBeInTheDocument();

		fireEvent.click(screen.getByText('Close edit'));
		expect(screen.queryByText('SkillEdit component')).toBeNull();
	});
});
