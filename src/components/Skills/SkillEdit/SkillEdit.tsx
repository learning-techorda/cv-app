import React from 'react';
import style from './SkillEdit.module.scss';
import Button from '../../Button/Button';
import { useDispatch } from 'react-redux';
import { addSkillAsync } from 'src/redux/skillSlice/thunk';
import { v4 as uuidv4 } from 'uuid';
import { AppDispatch } from 'src/store';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

const validationSchema = Yup.object({
	name: Yup.string().required('Skill name is a required field'),
	range: Yup.number()
		.typeError("Skill range must be a 'number' type")
		.min(10, 'Skill range must be greater than or equal to 10')
		.max(100, 'Skill range must be less than or equal to 100')
		.required('Skill range is a required field'),
});

interface FormValues {
	name: string;
	range: string;
}

const SkillEdit = () => {
	const dispatch = useDispatch<AppDispatch>();

	const initialValues: FormValues = { name: '', range: '' };

	const handleFormSubmit = (values, { setSubmitting }) => {
		const newSkill = {
			id: Number(uuidv4()),
			name: values.name,
			range: Number(values.range),
		};
		dispatch(addSkillAsync(newSkill));
		setSubmitting(false);
	};

	return (
		<div className={style.container}>
			<div className={style.inputsBox}>
				<Formik
					initialValues={initialValues}
					validationSchema={validationSchema}
					onSubmit={handleFormSubmit}
					enableReinitialize
				>
					{({ isValid, isSubmitting }) => (
						<Form className={style.box}>
							<label htmlFor='name'>Skill name:</label>
							<Field name='name'>
								{({ field, meta }) => {
									const inputStyle = {
										borderColor: meta.touched && meta.error ? 'red' : 'initial',
									};

									return (
										<input
											{...field}
											placeholder='Enter skill name'
											type='text'
											style={inputStyle}
											className={style.input}
										/>
									);
								}}
							</Field>
							<div style={{ color: 'red' }}>
								<ErrorMessage
									name='name'
									component='div'
									className={style.errorMessage}
								/>
							</div>
							<label htmlFor='range'>Skill range:</label>
							<Field name='range'>
								{({ field, meta }) => {
									const inputStyle = {
										borderColor: meta.touched && meta.error ? 'red' : 'initial',
									};
									return (
										<input
											{...field}
											style={inputStyle}
											className={style.input}
											placeholder='range'
										/>
									);
								}}
							</Field>
							<div style={{ color: 'red' }}>
								<ErrorMessage
									name='range'
									component='div'
									className={style.errorMessage}
								/>
							</div>
							<Button
								className={style.addSkillBtn}
								type='submit'
								children='Add skill'
								disabled={!isValid || isSubmitting}
							/>
						</Form>
					)}
				</Formik>
			</div>
		</div>
	);
};

export default SkillEdit;
