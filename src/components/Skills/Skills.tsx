import React, { useState } from 'react';
import SkillLevel from './SkillLevel/SkillLevel';
import SkillEdit from './SkillEdit/SkillEdit';
import style from './Skills.module.scss';
import Button from '../Button/Button';
import editIcon from '../../assets/images/skills/edit.png';
import SkillBlock from './SkillBlock/SkillBlock';

const Skills = () => {
	const [isShowSkillEdit, setIsShowSkillEdit] = useState(false);

	const openEditor = () => {
		setIsShowSkillEdit(!isShowSkillEdit);
	};
	return (
		<div className={style.container}>
			<div className={style.headerBox}>
				<h1 className={style.header}>Skills</h1>
				<Button className={style.btn} onClick={openEditor}>
					<img src={editIcon} className={style.editIcon} alt='Edit button' />
					{isShowSkillEdit ? 'Close edit' : 'Open edit'}
				</Button>
			</div>
			{isShowSkillEdit ? <SkillEdit /> : null}
			<SkillBlock />
			<SkillLevel />
		</div>
	);
};

export default Skills;
