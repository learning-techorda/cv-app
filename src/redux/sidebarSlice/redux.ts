import { createSlice } from '@reduxjs/toolkit';
import { initialStateProps } from './types';

export const initialState: initialStateProps = {
	isSidebarExpanded: true,
};

const sidebarReducer = createSlice({
	name: 'sidebar',
	initialState,
	reducers: {
		changeSidebar(state) {
			state.isSidebarExpanded = !state.isSidebarExpanded;
		},
	},
});

export default sidebarReducer.reducer;
export const { changeSidebar } = sidebarReducer.actions;
