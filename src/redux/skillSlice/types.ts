export interface SkillsProps {
	name: string;
	range: number;
	id: number;
}
