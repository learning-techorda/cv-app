import { createSlice } from '@reduxjs/toolkit';
import { SkillsProps } from './types';

const initialState: SkillsProps[] = [];

const skillsSlice = createSlice({
	name: 'skills',
	initialState,
	reducers: {
		setSkills: (_state, action) => {
			return action.payload;
		},
		addSkill: (state, action) => {
			state.push(action.payload);
		},
		removeSkill: (state, action) => {
			return state.filter((skill) => skill.id !== action.payload);
		},
	},
});

export const { setSkills, addSkill, removeSkill } = skillsSlice.actions;
export default skillsSlice.reducer;

export type SetSkillsAction = ReturnType<typeof setSkills>;
export type AddSkillAction = ReturnType<typeof addSkill>;
