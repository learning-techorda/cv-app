import { createAsyncThunk } from '@reduxjs/toolkit';
import { addSkill, removeSkill, setSkills } from './redux';
import { SkillsProps } from './types';

export const fetchSkills = createAsyncThunk(
	'skills/fetchSkills',
	async (_, { dispatch }) => {
		try {
			const response = await fetch('/api/skills');
			const data = await response.json();
			if (!Array.isArray(data)) {
				console.error('Expected an array', data);
			}
			dispatch(setSkills(data));
			return data;
		} catch (error) {
			console.error('Error fetching skills:', error);
		}
	}
);

export const addSkillAsync = createAsyncThunk<SkillsProps, SkillsProps>(
	'skills/addSkillAsync',
	async (newSkill, { dispatch }) => {
		const response = await fetch('/api/skills', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify(newSkill),
		});
		const data = await response.json();
		dispatch(addSkill(data));
		dispatch(fetchSkills());

		return data.result;
	}
);

export const deleteSkill = createAsyncThunk(
	'skills/deleteSkill',
	async (skillId: number, { dispatch }) => {
		const response = await fetch(`/api/skills/${skillId}`, {
			method: 'DELETE',
		});
		if (response.ok) {
			dispatch(fetchSkills());
			dispatch(removeSkill(skillId));
		}
		return skillId;
	}
);
