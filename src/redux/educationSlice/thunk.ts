import { createAsyncThunk } from '@reduxjs/toolkit';
import { Education } from './types';

export const fetchEducations = createAsyncThunk<Education[]>(
	'educations/fetchEducations',
	async (_, { rejectWithValue }) => {
		try {
			const response = await fetch('/api/educations');
			if (response.ok) {
				return await response.json();
			} else {
				return rejectWithValue(
					'Something went wrong; please review your server connection'
				);
			}
		} catch (error) {
			return rejectWithValue(
				'Something went wrong; please review your server connection'
			);
		}
	}
);
