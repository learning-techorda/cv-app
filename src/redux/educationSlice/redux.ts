import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { fetchEducations } from './thunk';
import { Education, initialState } from './types';

const educationsSlice = createSlice({
	name: 'educations',
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder
			.addCase(fetchEducations.pending, (state) => {
				state.loading = true;
				state.error = null;
			})
			.addCase(
				fetchEducations.fulfilled,
				(state, action: PayloadAction<Education[]>) => {
					state.educations = action.payload;
					state.loading = false;
				}
			)
			.addCase(fetchEducations.rejected, (state, action) => {
				state.loading = false;
				state.error = action.payload as string;
			});
	},
});

export default educationsSlice.reducer;
