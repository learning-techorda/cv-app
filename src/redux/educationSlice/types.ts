export interface Education {
	date: string;
	title: string;
	description: string;
}

export interface EducationState {
	educations: Education[];
	loading: boolean;
	error: string | null;
}

export const initialState: EducationState = {
	educations: [],
	loading: false,
	error: null,
};
