import React from 'react';
import PhotoBox from 'src/components/PhotoBox/PhotoBox';
import Button from 'src/components/Button/Button';
import user from 'src/assets/images/photoBox/User avatar.png';
import style from './Home.module.scss';
import { useNavigate } from 'react-router-dom';

const Home = () => {
	const name: string = 'John Doe';
	const title: string = 'Programmer. Creative. Innovator';
	const description: string =
		'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque';
	const navigate = useNavigate();

	const handleNavigateToInnerPage = () => {
		navigate('/inner');
	};

	return (
		<div className={style.home}>
			<div className={style.container}>
				<div className={style.photoBox}>
					<PhotoBox
						name={name}
						title={title}
						description={description}
						avatar={user}
						avatarClass={style.avatar}
						nameClass={style.name}
						titleClass={style.title}
						descriptionClass={style.description}
					/>
					<Button
						className={style.btn}
						onClick={handleNavigateToInnerPage}
						children='Know more'
					/>
				</div>
			</div>
		</div>
	);
};

export default Home;
