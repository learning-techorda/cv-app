import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Button from 'src/components/Button/Button';
import style from './Home.module.scss';
import { MemoryRouter } from 'react-router-dom';
import Home from './Home';

const navigateByMock = jest.fn();

jest.mock('react-router-dom', () => {
	const actualReactRouterDOM = jest.requireActual('react-router-dom');
	return {
		...actualReactRouterDOM,
		useNavigate: jest.fn(),
	};
});

describe('Home component ', () => {
	beforeEach(() => {
		render(
			<MemoryRouter>
				<Home />
			</MemoryRouter>
		);
	});

	test('displays information in PhotoBox', () => {
		expect(screen.getByText('John Doe')).toBeInTheDocument();
		expect(
			screen.getByText('Programmer. Creative. Innovator')
		).toBeInTheDocument();
		expect(
			screen.getByText(
				'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque'
			)
		).toBeInTheDocument();
		expect(screen.getByAltText('Avatar')).toBeInTheDocument();
	});
});
describe('Button component tests', () => {
	beforeEach(() => {
		navigateByMock.mockClear();
		render(
			<MemoryRouter>
				<Button
					className={style.btn}
					onClick={() => navigateByMock('/inner')}
					children='Know more'
				/>
			</MemoryRouter>
		);
	});

	test('Button is rendered and visible on the screen', () => {
		const buttonElement = screen.getByText('Know more');
		expect(buttonElement).toBeInTheDocument();
	});

	test('Button has the correct class', () => {
		const buttonElement = screen.getByText('Know more');
		expect(buttonElement).toHaveClass(style.btn);
	});

	test('Button click navigate to the inner page', () => {
		const buttonElement = screen.getByText('Know more');
		fireEvent.click(buttonElement);
		expect(navigateByMock).toHaveBeenCalledWith('/inner');
		expect(navigateByMock).toHaveBeenCalledTimes(1);
	});
});
