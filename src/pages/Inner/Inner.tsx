import React, { useRef, FC } from 'react';
import Panel from 'src/components/Panel/Panel';
import Box from 'src/components/Box/Box';
import { aboutMeHeader, aboutMeParagraph } from 'src/constants';
import style from './Inner.module.scss';
import buttonUp from 'src/assets/images/navigation/angle-left-solid.svg';
import { ScrollContext } from '../../ScrollContext';
import Expertise from 'src/components/Expertise/Expertise';
import Skills from 'src/components/Skills/Skills';
import Portfolio from 'src/components/Portfolio/Portfolio';
import Address from 'src/components/Address/Address';
import Feedback from 'src/components/Feedback/Feedback';
import EducationList from 'src/components/TimeLine/EducationList/EducationList';
import Button from 'src/components/Button/Button';

const Inner: FC = () => {
	const aboutMeRef = useRef<HTMLDivElement>(null);
	const educationSectionRef = useRef<HTMLDivElement>(null);
	const experienceSectionRef = useRef<HTMLDivElement>(null);
	const skillsSectionRef = useRef<HTMLDivElement>(null);
	const portfolioSectionRef = useRef<HTMLDivElement>(null);
	const contactsSectionRef = useRef<HTMLDivElement>(null);
	const feedbackSectionRef = useRef<HTMLDivElement>(null);
	const contentRef = useRef<HTMLDivElement>(null);

	const scrollToSection = (id: string) => {
		const section = {
			aboutMeSection: aboutMeRef.current,
			educationSection: educationSectionRef.current,
			experienceSection: experienceSectionRef.current,
			skillsSection: skillsSectionRef.current,
			portfolioSection: portfolioSectionRef.current,
			contactsSection: contactsSectionRef.current,
			feedbackSection: feedbackSectionRef.current,
		}[id];
		section?.scrollIntoView({ behavior: 'smooth' });
	};
	const scrollToTop = () => {
		if (contentRef.current) {
			contentRef.current.scrollTo({
				top: 0,
				behavior: 'smooth',
			});
		}
	};

	return (
		<ScrollContext.Provider value={{ scrollToSection }}>
			<div className={style.innerContainer}>
				<div className={style.sidebar}>
					<Panel />
				</div>
				<div className={style.content} ref={contentRef}>
					<div ref={aboutMeRef} id='aboutMeSection'>
						<Box header={aboutMeHeader} paragraph={aboutMeParagraph} />
					</div>
					<div ref={educationSectionRef} id='educationSection'>
						<EducationList />
					</div>
					<div ref={experienceSectionRef} data-testid='experienceSection'>
						<Expertise />
					</div>
					<div ref={skillsSectionRef} id='skillsSection'>
						<Skills />
					</div>
					<div ref={portfolioSectionRef} id='portfolioSection'>
						<Portfolio />
					</div>
					<div ref={contactsSectionRef} id='contactsSection'>
						<Address />
					</div>
					<div ref={feedbackSectionRef} id='feedbackSection'>
						<Feedback />
					</div>
				</div>
				<Button
					className={style.btnUp}
					onClick={scrollToTop}
					data-testid='scrollToTopButton'
				>
					<img className={style.btnUpIcon} src={buttonUp} alt='Go to Up' />
				</Button>
			</div>
		</ScrollContext.Provider>
	);
};

export default Inner;
