import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Inner from './Inner';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import { ScrollContext } from 'src/ScrollContext';
import store from '../../store';
import Button from 'src/components/Button/Button';

jest.mock('src/components/Panel/Panel', () => () => <div>Panel</div>);
jest.mock('src/components/Box/Box', () => () => <div>Box</div>);
jest.mock('src/components/Expertise/Expertise', () => () => (
	<div>Expertise</div>
));
jest.mock('src/components/Skills/Skills', () => () => <div>Skills</div>);
jest.mock('src/components/Portfolio/Portfolio', () => () => (
	<div>Portfolio</div>
));
jest.mock('src/components/Address/Address', () => () => <div>Address</div>);
jest.mock('src/components/Feedback/Feedback', () => () => <div>Feedback</div>);
jest.mock('src/components/TimeLine/EducationList/EducationList', () => () => (
	<div>EducationList</div>
));

beforeAll(() => {
	window.scrollTo = jest.fn();
	Element.prototype.scrollTo = jest.fn();
});

describe('Inner Component', () => {
	it('renders all sections and components correctly', () => {
		const { getByText } = render(
			<Provider store={store}>
				<MemoryRouter>
					<ScrollContext.Provider value={{ scrollToSection: jest.fn() }}>
						<Inner />
					</ScrollContext.Provider>
				</MemoryRouter>
			</Provider>
		);

		expect(getByText('Panel')).toBeInTheDocument();
		expect(getByText('Box')).toBeInTheDocument();
		expect(getByText('Expertise')).toBeInTheDocument();
		expect(getByText('Skills')).toBeInTheDocument();
		expect(getByText('Portfolio')).toBeInTheDocument();
		expect(getByText('Address')).toBeInTheDocument();
		expect(getByText('Feedback')).toBeInTheDocument();
		expect(getByText('EducationList')).toBeInTheDocument();
	});
});

describe('Button Component', () => {
	it('simulates click event', () => {
		const { getByRole } = render(
			<MemoryRouter>
				<Button
					className='btnUp'
					onClick={() => window.scrollTo({ top: 0, behavior: 'smooth' })}
					data-testid='scrollToTopButton'
				>
					<img
						src='src/assets/images/navigation/angle-left-solid.svg'
						alt='Go to Up'
					/>
				</Button>
			</MemoryRouter>
		);

		const upButton = getByRole('button');
		fireEvent.click(upButton);

		expect(window.scrollTo).toHaveBeenCalledWith({
			top: 0,
			behavior: 'smooth',
		});
	});
});
