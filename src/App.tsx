import React from 'react';
import './App.scss';
import Home from '../src/pages/Home/Home';
import Inner from '../src/pages/Inner/Inner';
import { Route, Routes } from 'react-router-dom';
function App() {
	return (
		<div className='App'>
			<Routes>
				<Route path='/' element={<Home />} />
				<Route path='/inner' element={<Inner />} />
			</Routes>
		</div>
	);
}

export default App;
