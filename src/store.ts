import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './rootReducer';
import { localStorageMiddleware } from './middleware/localStorageMiddleware';

const preloadedState = {
	skills: JSON.parse(localStorage.getItem('skills')),
};
console.log('Loaded skills from localStorage:', preloadedState.skills);

const store = configureStore({
	reducer: rootReducer,
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware().concat(localStorageMiddleware),
	preloadedState,
});
export default store;
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof rootReducer>;
