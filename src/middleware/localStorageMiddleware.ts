import { Middleware, Dispatch } from 'redux';
import { RootState } from '../store';
import { SetSkillsAction, setSkills } from '../redux/skillSlice/redux';

type SkillAction = SetSkillsAction;

export const localStorageMiddleware: Middleware<
	Record<string, never>,
	RootState,
	Dispatch<SkillAction>
> = (store) => (next) => (action: SkillAction) => {
	const result = next(action);
	if (action.type === setSkills.type) {
		localStorage.setItem('skills', JSON.stringify(action.payload));
	} else if (action.type === 'skills/deleteSkill/fulfilled') {
		localStorage.removeItem('skills');
	}
	return result;
};
