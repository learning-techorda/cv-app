import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import App from './App';

jest.mock('../src/pages/Home/Home', () => () => <div>Home Component</div>);
jest.mock('../src/pages/Inner/Inner', () => () => <div>Inner Component</div>);

describe('App Component', () => {
	it('should render Home component for default route', () => {
		render(
			<MemoryRouter initialEntries={['/']}>
				<App />
			</MemoryRouter>
		);
		expect(screen.getByText('Home Component')).toBeInTheDocument();
	});

	it('should render Inner component for "/inner" route', () => {
		render(
			<MemoryRouter initialEntries={['/inner']}>
				<App />
			</MemoryRouter>
		);
		expect(screen.getByText('Inner Component')).toBeInTheDocument();
	});
});
