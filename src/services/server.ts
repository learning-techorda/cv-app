import { createServer, Model, Response } from 'miragejs';

interface Education {
	date: string;
	title: string;
	description: string;
}
interface Skill {
	id: string;
	name: string;
	level: number;
}

export function makeServer({ environment = 'development' } = {}) {
	return createServer({
		environment,

		models: {
			education: Model.extend<Partial<Education>>({}),
			skill: Model.extend<Partial<Skill>>({}),
		},

		seeds(server) {
			const storedSkills = localStorage.getItem('skills');

			if (!storedSkills) {
				server.db.loadData({
					skills: [
						{
							id: '1',
							name: 'JavaScript',
							range: 90,
						},
					],
				});
			} else {
				server.db.loadData({
					skills: JSON.parse(storedSkills),
				});
			}
			server.db.loadData({
				educations: [
					{
						date: 2001,
						title: 'Title 0',
						description:
							'Elit voluptate ad nostrum Laboris. Elit incididunt mollit enim enim id id Laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat minim do aliqua amet ex dolore velit. \r \n ',
					},
					{
						date: 2000,
						title: 'Title 1 ',
						description:
							'Et culpa ad proident Labore Executeur Elit Dolore. Quis commodo elit culpa eiusmod dolor proident not commodo replaceur aute duis duis eu fugiat. Eu duis occaecat nulla eiusmod non esse cillum est aute elit amet cillum commodo \r . \n ',
					},
					{
						date: 2012,
						title: 'Title 2',
						description:
							'Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrum. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod. aliquip voluptate Велит великий труд эйусмод эйусмод труд амет эйусмод. Reprehenderit fugiat amet sint commodo ex. \r \n ',
					},
					{
						date: 2013,
						title: 'Title 3',
						description:
							'Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrum. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod. aliquip voluptate Велит великий труд эйусмод эйусмод труд амет эйусмод. Reprehenderit fugiat amet sint commodo ex. \r \n ',
					},
					{
						date: 2014,
						title: 'Title 4',
						description:
							'Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrum. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod. aliquip voluptate Велит великий труд эйусмод эйусмод труд амет эйусмод. Reprehenderit fugiat amet sint commodo ex. \r \n ',
					},
					{
						date: 2015,
						title: 'Title 6',
						description:
							'Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrum. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod. aliquip voluptate Велит великий труд эйусмод эйусмод труд амет эйусмод. Reprehenderit fugiat amet sint commodo ex. \r \n ',
					},
					{
						date: 2016,
						title: 'Title 7',
						description:
							'Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrum. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod. aliquip voluptate Велит великий труд эйусмод эйусмод труд амет эйусмод. Reprehenderit fugiat amet sint commodo ex. \r \n ',
					},
					{
						date: 2017,
						title: 'Title 8',
						description:
							'Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrum. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod. aliquip voluptate Велит великий труд эйусмод эйусмод труд амет эйусмод. Reprehenderit fugiat amet sint commodo ex. \r \n ',
					},
					{
						date: 2017,
						title: 'Title 9',
						description:
							'Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrum. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod. aliquip voluptate Велит великий труд эйусмод эйусмод труд амет эйусмод. Reprehenderit fugiat amet sint commodo ex. \r \n ',
					},
				],
			});
		},

		routes() {
			this.namespace = 'api';

			this.get(
				'/educations',
				(schema) => {
					// return new Response(500, {}, { errors: ['name cannot be blank'] });

					return schema.db.educations;
				},
				{ timing: 3000 }
			);

			this.get('/skills', (schema) => {
				return schema.db.skills;
			});

			this.post('/skills', (schema, request) => {
				const newSkill = JSON.parse(request.requestBody);
				schema.db.skills.insert(newSkill);
				return schema.db.skills;
			});

			this.delete('/skills/:id', (schema, request) => {
				const id = request.params.id;
				schema.db.skills.remove(id);
				return {};
			});
		},
	});
}
